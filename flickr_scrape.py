import flickrapi
import urllib.request
from PIL import Image
import random
'''obtain api key and secret from flickr'''

key = ''
secret =  ''

flickr =flickrapi.FlickrAPI(key,secret)

desired_size = 350 #Size required by model

photos = flickr.walk(extras = 'url_c',
                     per_page = 12000,
                     sort = 'relevance',
                     license="1,2,4,5,6,7",
                     ispublic="1",
                     content_type = 1,
                     group_id = '341767@N20'
                     )

urls = []

for i, photo in enumerate(photos):
    url = photo.get('url_c')
    urls.append(url)
    if i> 600:
        break
    
for x in range(600):
    filepath = str(x + 3500) + '.jpg'
    try:
        urllib.request.urlretrieve(urls[x], filepath)
        image = Image.open(filepath)
        old_size = image.size
        ratio = float(desired_size) / max(old_size)
        new_size = tuple([int(x*ratio) for x in old_size])
        image = image.resize(new_size, Image.ANTIALIAS)
        image = image.rotate(45)
        #image = image.traspose(Image.FLIP_TOP_BOTTOM)
        new_im = Image.new("RGB", (desired_size, desired_size))
        new_im.paste(image, ((desired_size - new_size[0])//2,
                             (desired_size-new_size[1])//2))
        image = new_im
      
       
        image.save(filepath)
    except:
        pass
   
